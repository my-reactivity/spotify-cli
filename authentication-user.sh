#!/usr/bin/env bash

# EXPECT variable enironment
# SP_ID
# SP_SECRET

PORT=8998
COOKIE_FILE="$HOME/.config/spotify-cli/.sp-cli.cookie"

SCOPES='playlist-read-private,user-modify-playback-state,user-read-playback-state,user-read-private'
STATE="sp-cli"

callback_url="http%3A%2F%2Flocalhost%3A8998%2Fcallback"
token_url="https://accounts.spotify.com/api/token"
authorize_url="https://accounts.spotify.com/authorize"
user_info_url="https://api.spotify.com/v1/me"

path=
authorization_code=

function parse_http_request {
  local path
  # an http (v 1.1) request looks like this:
  #
  #  METHOD /path HTTP/1.1
  #  Header: value
  #  Other-heder: value
  #  Cookie: here-comes-the-cookies
  #
  #  request body
  #
  # the headers are separated from the body by an empty line
  # we are interested in the path
  read _ path _

  # we are also interested in the "bashsessionid" session cookie
  while read var
  do
    var=$(echo $var | tr -d '\r\n')
  
    if [ -z "${var}" ]; then
      break
    fi
  done
  echo $path
}

function render_start_page {
  if [[ -f ${COOKIE_FILE} ]]; then
    # We have an active session - the session cookie provided by the browser points
    # to an access token stored on disk
    . ${COOKIE_FILE}
    if [[ -z "${ACCESS_TOKEN}"  ]] ; then
      render_sign_in
    else 
      # fetch the user info resource
      user_info=$(curl -s -X GET -H "Authorization: Bearer ${ACCESS_TOKEN}" ${user_info_url})
      # check if error
      error=".*\"error\": .*"
      if [[ "${user_info}" =~ ${error} ]] ; then
        render_sign_in
      else
        render_user_details
      fi
    fi
  else
    render_sign_in
  fi

}

function render_user_details () {
  echo "HTTP/1.1 200 OK
Content-Type: text/html

<html>
<head><title>Spotify CLI login</title></head>
<body>"
  # and regex out the user's name
  user_name_re=".*\"displayName\": \"([^\"]+)\".*"
  if [[ "${user_info}" =~ $user_name_re ]]; then
    user_name=${BASH_REMATCH[1]}
    echo "<p>You are signed in as ${user_name}.</p>"
  else
    echo "<pre>${user_info}</pre>"
  fi
  echo "<strong>You could close this window now and enjoy spotify-cli</strong>"
  echo "</body></html>
  "
}

function render_sign_in {
  authorize_parameters="response_type=code&client_id=${SP_ID}&scope=${SCOPES}&state=${STATE}&redirect_uri=${callback_url}"
  echo "HTTP/1.1 302 Found
Location: ${authorize_url}?${authorize_parameters}
"
}

function render_sign_in_callback {
  # exchange authorization code for an access token
  if [[ $(refresh_token "authorization_code" "&code=${authorization_code}") == "0" ]] ; then
  # we are redirecting using meta because the browser tries to
  # load regular redirect targets too quickly for our netcat server
  echo "HTTP/1.1 200 OK
Content-Type: text/html

<html><head><meta http-equiv=\"refresh\" content=\"1;URL=/\"></head></html>
"
  else
    echo "HTTP/1.1 400 OK
Content-Type: application/json

${at_response}
# 
"
  fi

}

function render_404 {
  echo "HTTP/1.1 404 Not Found
Content-Type: text/html

<html>
<head><title>Spotify CLI Page Not Found</title></head>
<body>
  <h1>404 Not Found</h1>
  <p>Resource \"${path}\" could not be found.</p>
</body></html>
"
}

function server () {
  path=$(parse_http_request)

  # here we do our routing
  if [ "$path" == "/" ]; then
    render_start_page
  elif [ "$path" == "/sign-in" ]; then
    render_sign_in
  elif [[ "$path" =~ ^/callback\?code=(.+)$ ]]; then
    authorization_code=${BASH_REMATCH[1]}
    render_sign_in_callback
  else
    render_404
  fi
}

function refresh_token () {
  GRANT_TYPE="$1"
  if [[ -z "$GRANT_TYPE" ]] ; then
    GRANT_TYPE="refresh_token"
  fi
  token_page_parameters="client_id=${SP_ID}&client_secret=${SP_SECRET}&redirect_uri=${callback_url}&grant_type=${GRANT_TYPE}$2"
  at_response=$(curl -s -X POST -d "${token_page_parameters}" ${token_url})

  at_re=".*\"access_token\" *: *\"([^\"]+)\".*"
  if [[ "${at_response}" =~ $at_re ]]; then
    # yep, we got an access token
    ACCESS_TOKEN=${BASH_REMATCH[1]}
    echo "ACCESS_TOKEN=$ACCESS_TOKEN" > ${COOKIE_FILE}
    
    at_refresh=".*\"refresh_token\" *: *\"([^\"]+)\".*"
    if [[ "${at_response}" =~ $at_refresh ]]; then
      # yep, we got an refresh token
      refresh_token=${BASH_REMATCH[1]}
      echo "REFRESH_TOKEN=$refresh_token" >> ${COOKIE_FILE}
    elif [[ ! -z "${REFRESH_TOKEN}" ]] ; then
      echo "REFRESH_TOKEN=$REFRESH_TOKEN" >> ${COOKIE_FILE}
    fi
    echo 0
  else
    ACCESS_TOKEN=''
    REFRESH_TOKEN=''
    rm -rf "${COOKIE_FILE}"
    echo 1
  fi
}

function load_cookie () {
  if [[ -e ${COOKIE_FILE} ]] ; then
    . ${COOKIE_FILE}
  fi
}

function authenticate_user () {
  load_cookie
  if [[ $(refresh_token "refresh_token" "&refresh_token=${REFRESH_TOKEN}" ) == "1" ]] ; then
    xdg-open http://localhost:${PORT} & start_server
  fi
  load_cookie
  echo ${ACCESS_TOKEN}
}
alias sp-authenticate_user="authenticate_user"

function start_server {
  # shellcheck disable=SC1083
  exec {fd}<> <(:)
  load_cookie
  while [[ -z  "${ACCESS_TOKEN}" ]] ; do
    # shellcheck disable=SC2154
    server <&$fd | nc -q 0 -l ${PORT}  >&$fd 
    load_cookie
  done
  # still activate the server one last time to display the information to the user
  server <&$fd | nc -q 0 -l ${PORT}  >&$fd
  # shellcheck disable=SC1083
  exec {fd}<&-
}
alias sp-start_server="start_server"

function sp-check_user_authentication () {
  load_cookie
  if [[ $(refresh_token "refresh_token" "&refresh_token=${REFRESH_TOKEN}" ) == "1" ]] ; then
    exit 1
  else
    exit 0
  fi
}