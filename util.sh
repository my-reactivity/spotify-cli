#!/usr/bin/env bash

# UTILITY FUNCTIONS

function require {
  hash $1 2>/dev/null || {
    echo >&2 "Error: '$1' is required, but was not found."; exit 1;
  }
}

function log {
  if [[ ! -z "${DEBUG}" ]] ; then
    echo "$*"
  fi  
}

if [[ ! -z "${DEBUG}" ]] ; then
  if [[ "${DEBUG}" == "all" ]] ; then
    echo "DEBUG activated"
    sp-version
    set -x
  fi
fi
