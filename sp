#!/usr/bin/env bash

# Edited to allow for authentication with Spotify API required for search function.
# Register your application here: https://beta.developer.spotify.com/dashboard/applications  and insert CLient ID and Secret below.
# mainly inspired by https://gist.github.com/wandernauta/6800547

dir=$(dirname $0)

. ${dir}/help.sh
. ${dir}/util.sh

# COMMON REQUIRED BINARIES
# Assert standard Unix utilities are available.
require grep
require sed
require cut
require tr

. ${dir}/authentication.sh
. ${dir}/command.sh

# Then we dispatch the command.
dispatch "$@"
