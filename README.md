# Spotify-cli for linux

This work was started from https://gist.github.com/wandernauta/6800547 and the existing forks.

Now there is no use of dbus to manipulate spotify but only the [Web API](https://developer.spotify.com/web-api/) provided by spotify.

# How to install it

To be able to use this CLI, you need to have a spotify premium account.

Then you need to have a player opened (one handled by the Web API). 
At least Official clients on desktop and android are this kind of player.
I guess this is also the case for the IOS app but I don't have a way to test it.

## Dependencies

You need at least the following commands: `dir`, `grep`, `sed`, `cut`, `tr`.

**They should be available on almost *NIX system.**


`curl` is also required as we call a web API to manipulate Spotify clients.

The following are optional but strongly recommended to be able to use the full potential of the tool:
- `column` to display the metadata about the current song.
- `feh` to display art

column should be also available on almost *NIX system.

If you want to install the missing one, please have a look on the documentation of your distro.

# How to use it

## Configuration

Here is a copy of the help display called with `sp help-configuration`:
```
At least two variables need to be defined to ensure the correct behavior of all operations allowed with this CLI

You can define `$HOME/.config/spotify-cli/configuration` with such a content:
SP_ID="<shoud be replaced>"
SP_SECRET="<should be replaced>"

Register the app and get the ID and SECRET token here : https://beta.developer.spotify.com/dashboard/

Another possibility is to defined this variables in your environment or even give them on the command line:
SP_ID="<shoud be replaced>" SP_SECRET="<should be replaced>" sp [command]

Once this is done, you need to authentication you user.
To do so, run sp-authenticate_user
```

## Available commands

Here is a copy of the help display called with `sp help`
```
Usage: sp [command]
Control a running Spotify instance from the command line.

  sp play                                     - Play Spotify
  sp pause                                    - Pause Spotify
  sp next                                     - Go to next track
  sp prev                                     - Go to previous track
  sp previous                                 - Go to previous track

  sp current                                  - Format the currently playing track
  sp metadata                                 - Dump the current track's metadata
  sp eval                                     - Return the metadata as a shell script

  sp art                                      - Print the URL to the current track's album artwork
  sp display                                  - Display the current album artwork with `feh`
  sp display-timeout                          - Display the current album artwork with `feh` and display it only 2s

  sp play <uri>                               - Open and play a spotify: uri
  sp search <q>                               - Start playing the best search result (as track) for the given query
  sp album <q>                                - Start playing the best search result (as album) for the given query
  sp artist <q>                               - Start playing the best search result (as artist) for the given query
  sp list <q> ...                             - Start playing the best search result (as playlist) for the given query
  sp playlist <q> ...                         - Start playing the best search result (as playlist) for the given query
  sp my-list <q> ...                          - Start playing the best search result (as playlist on the user's playlists) for the given query
  sp my-playlist <q> ...                      - Start playing the best search result (as playlist on the user's playlists) for the given query

  sp play-from <artist name> ...              - Start playing a set of recommendations from the given artists

  sp url                                      - Print the HTTP URL for the currently playing track
  sp clip                                     - Copy the HTTP URL to the X clipboard
  sp http                                     - Open the HTTP URL in a web browser

  sp version                                  - Show version information
  sp help                                     - Show this information

Any other argument will start a search (i.e. 'sp foo' will search for foo).
```