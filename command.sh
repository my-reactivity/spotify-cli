#!/usr/bin/env bash

dir=$(dirname $0)

BASE_URL_API="https://api.spotify.com/v1"

function prepare_uniq_uri {
  echo "{\"context_uri\":$1}"
}

function prepare_multiple_uris {
  echo "{\"uris\":[$1]}"
}

function sp-play {
  # Opens the given spotify: URI(s) in Spotify.
  local SPTFY_URI="$1"
  log "${SPTFY_URI}"

  ST2=$(authenticate_user)
  if [[ -z "${SPTFY_URI}" ]] ; then
  curl -s -X PUT -H "Authorization: Bearer $ST2" ${BASE_URL_API}/me/player/play
  else
    curl -s -X PUT -H "Authorization: Bearer $ST2" -d "${SPTFY_URI}" ${BASE_URL_API}/me/player/play
  fi
}

function sp-pause {
  ST2=$(authenticate_user)
  curl -s -X PUT -H "Authorization: Bearer $ST2" ${BASE_URL_API}/me/player/pause
}

function sp-previous {
  ST2=$(authenticate_user)
  curl -s -X POST -H "Authorization: Bearer $ST2" ${BASE_URL_API}/me/player/previous
}

alias sp-prev="sp-previous"

function sp-next {
  ST2=$(authenticate_user)
  curl -s -X POST -H "Authorization: Bearer $ST2" ${BASE_URL_API}/me/player/next
}
function sp-metadata {
  ST2=$(authenticate_user)
  curl -s -X GET -H "Authorization: Bearer $ST2" "${BASE_URL_API}/me/player/currently-playing"
}

function sp-current {
  # Prints the currently playing track in a friendly format.
  require column

  metadata=$(sp-metadata)

  local current=""
  current="Title|$(echo "${metadata}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.name")"
  log "$current" | column -t -s'|'
  current="${current}\nArtist(s)|$(echo "${metadata}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.artists..name" | tr '\n' ',' | sed 's/,$//')"
  log "$current" | column -t -s'|'
  current="${current}\nAlbum|$(echo "${metadata}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.album.name" | sed 's/,$//')"
  log "$current" | column -t -s'|'
  echo -e "$current" | column -t -s'|'

}

function sp-eval {
  # Prints the currently playing track as shell variables, ready to be eval'ed
  metadata=$(sp-metadata)
  echo "SPOTIFY_TITLE=\"$(echo "${metadata}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.name" | sed 's/,$//')\""
  echo "SPOTIFY_ARTIST=\"$(echo "${metadata}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.artists..name" | tr '\n' ',' | sed 's/,$//')\""
  echo "SPOTIFY_ALBUM=\"$(echo "${metadata}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.album.name" | sed 's/,$//')\""
  echo "SPOTIFY_ID=\"$(echo "${metadata}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.uri" | sed 's/,$//')\""
  echo "SPOTIFY_TRACKNUMBER=\"$(echo "${metadata}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.track_number" | sed 's/,$//')\""
}

# art URL for bigger image
function sp-art {
	# Prints the artUrl.
	sp-metadata | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.album.images[0].url" | cut -d'|' -f2 | sed 's|open.spotify.com|i.scdn.co|g'
}

function sp-display {
  # Calls feh on the artURl.
  require feh
  feh -x "$(sp-art)"
}

function sp-display-timeout {
  # Calls feh on the artURl and display it only 2s.
  require feh
  timeout -k 2s 2s feh -x "$(sp-art)"
}

function sp-url {
  # Prints the HTTP url.
  sp-metadata | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "item.href"
}

function sp-clip {
  # Copies the HTTP url.
  require xclip
  sp-url | xclip
}

function sp-http {
  # xdg-opens the HTTP url.

  require xdg-open
  xdg-open "$(sp-url)"
}


function sp-my-list {
	# Searches for playlists, plays the first result.
	require curl
	Q="$*"

  ST2=$(authenticate_user)

  SPTFY_URI=$( \
    curl -s -H "Authorization: Bearer $ST2" -G ${BASE_URL_API}/me/playlists \
    | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "items[?(@.name==\".*$Q.*\")].uri" \
  )

	sp-play "$(prepare_uniq_uri "\"${SPTFY_URI}\"")"
}
alias sp-my-playlist="sp-my-list"

function sp-list {
	# Searches for playlists, plays the first result.
	Q="$*"

  SPTFY_URI=$( \
		internal_search playlist $Q \
		| grep -E -o "spotify:user:.*:playlist:[a-zA-Z0-9]+" -m 1 \
	)

	sp-play "$(prepare_uniq_uri "\"${SPTFY_URI}\"")"
}
alias sp-playlist="sp-list"

function sp-album {
	# Searches for albums, plays the first result.
	Q="$*"

	SPTFY_URI=$( \
		internal_search album "$Q" \
		| grep -E -o "spotify:album:[a-zA-Z0-9]+" -m 1 \
	)
	sp-play "$(prepare_uniq_uri "\"${SPTFY_URI}\"")"
}

function sp-artist {
	# Searches for artists, plays the first result.
	Q="$*"

	SPTFY_URI=$( \
		internal_search artist $Q \
		| grep -E -o "spotify:artist:[a-zA-Z0-9]+" -m 1 \
	)
	sp-play "$(prepare_uniq_uri "\"${SPTFY_URI}\"")"
}

function sp-search {
  # Searches for tracks, plays the first result.
  Q="$*"

  SPTFY_URI=$( \
    internal_search track "$Q" \
    | grep -E -o "spotify:track:[a-zA-Z0-9]+" -m 1 \
  )

  sp-play "$(prepare_uniq_uri "\"${SPTFY_URI}\"")"
}

function sp-play-from {
  # Searches for playlists, plays the first result.

	search_list=( "$@" )
  search=""

  ST2=$(authenticate_app)

  for s in "${search_list[@]}" ; do
    search_link=$( \
  		internal_search artist $s \
  		| grep -E -o "spotify:artist:[a-zA-Z0-9]+" -m 1 | sed "s/spotify:artist://g" \
  	)
    if [[ ! -z ${search_link} ]] ; then
      if [[ -z ${search} ]] ; then
        search="${search_link}"
      else
      search="${search},${search_link}"
      fi
    fi
  done

  # TODO limit recommendations on market of the user
  SPTFY_URI="\""$( \
		curl -s -H "Authorization: Bearer $ST2" -G --data-urlencode "seed_artists=${search}" ${BASE_URL_API}/recommendations?limit=100 \
    | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "tracks.*.uri" | tr '\n' ','  | sed 's/,/","/g' | sed 's/,"$//' \
	)
  sp-play "$(prepare_multiple_uris "${SPTFY_URI}")"
}

function sp-players {
  require curl

  ST2=$(authenticate_user)

  devices=$( \
    curl -s -H "Authorization: Bearer $ST2" -X GET "${BASE_URL_API}/me/player/devices" \
  )
  if [[ -z "$1" ]] ; then
    echo -e "${devices}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "devices.*.name"
  else
    device_name="$1"
    device_id=$(echo -e "${devices}" | bash ${dir}/JSONPath.sh/JSONPath.sh -b -i "devices[?(@.name=${device_name})].id")
    curl -s -H "Authorization: Bearer $ST2" -X PUT --data "{\"device_ids\":[\"${device_id}\"]}" "${BASE_URL_API}/me/player"
  fi
}

function internal_search {
  require curl

  ST2=$(authenticate_user)
  curl -s -H "Authorization: Bearer $ST2" -G --data-urlencode "q=$2" "${BASE_URL_API}/search?market=from_token&type=$1"
}

function dispatch {
  subcommand="$1"

  if [[ -z "$subcommand" ]]; then
    # No arguments given, print help.
    sp-help
  else
    # Arguments given, check if it's a command.
    type sp-$subcommand > /dev/null 2> /dev/null
    if [[ "$?" -eq "0" ]] ; then
      # It is. Run it.
      shift
      eval "sp-$subcommand $*"
    else
      # It's not. Try a search.
      eval "sp-search $*"
    fi
  fi
}
