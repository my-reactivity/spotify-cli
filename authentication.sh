#!/usr/bin/env bash

# EXPECT environment variables
# SP_ID		#your API Client ID
# SP_SECRET #your API Client Secret

dir=$(dirname $0)

shopt -s expand_aliases

. ${dir}/authentication-user.sh

function authenticate_app () {
  local ST
  #send request for token with ID and SecretID encoded to base64->grep take only  token from reply->trim reply down to token-> modified request to include token in header
  ST=$(curl -X "POST" -H "Authorization: Basic $SP_B64ID" -s -d grant_type=client_credentials https://accounts.spotify.com/api/token \
    | grep -E -o "\"access_token\":\"[a-zA-Z0-9_-]+\"" -m 1)

  at_re=".*\"access_token\" *: *\"([^\"]+)\".*"
  if [[ "${ST}" =~ $at_re ]]; then
    echo "${BASH_REMATCH[1]}"
  else
    echo ""
  fi
}

function prepare_configuration {
  . ${dir}/CONFIG

  [[ -e "${HOME}/.config/spotify-cli/configuration" ]] && . ${HOME}/.config/spotify-cli/configuration

  SP_B64ID=$(echo -n "$SP_ID:$SP_SECRET"|base64)
  SP_B64ID=$(echo $SP_B64ID | sed 's/ //g')

  if [[ -z $SP_ID || -z $SP_SECRET ]]; then
    sp-help-configuration
  fi
}

prepare_configuration
