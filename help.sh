#!/usr/bin/env bash

dir=$(dirname $0)
. ${dir}/VERSION

function sp-version {
  # Prints version information.

  echo "sp $SP_VERSION"
  echo "License GPL"
}

function sp-help-configuration {
  # Prints usage information.

  echo "At least two variables need to be defined to ensure the correct behavior of all operations allowed with this CLI"
  echo ""
  echo "You can define ${HOME}/.config/spotify-cli/configuration with such a content:"
  echo "SP_ID=\"<shoud be replaced>\""
  echo "SP_SECRET=\"<should be replaced>\""
  echo ""
  echo "Register the app and get the ID and SECRET token here : https://beta.developer.spotify.com/dashboard/"
  echo ""
  echo "Another possibility is to defined this variables in your environment or even give them on the command line:"
  echo "SP_ID=\"<shoud be replaced>\" SP_SECRET=\"<should be replaced>\" sp [command]"
  echo ""
  echo "Once this is done, you need to authentication you user."
  echo "To do so, run sp-authenticate_user"
}

function sp-help {
  # Prints usage information.

  echo "Usage: sp [command]"
  echo "Control a running Spotify instance from the command line."
  echo ""
  echo "  sp play                                     - Play Spotify"
  echo "  sp pause                                    - Pause Spotify"
  echo "  sp next                                     - Go to next track"
  echo "  sp prev                                     - Go to previous track"
  echo "  sp previous                                 - Go to previous track"
  echo ""
  echo "  sp current                                  - Format the currently playing track"
  echo "  sp metadata                                 - Dump the current track's metadata"
  echo "  sp eval                                     - Return the metadata as a shell script"
  echo ""
  echo "  sp art                                      - Print the URL to the current track's album artwork"
  echo "  sp display                                  - Display the current album artwork with \`feh\`"
  echo "  sp display-timeout                          - Display the current album artwork with \`feh\` and display it only 2s"
  echo ""
  echo "  sp play <uri>                               - Open and play a spotify: uri"
  echo "  sp search <q>                               - Start playing the best search result (as track) for the given query"
  echo "  sp album <q>                                - Start playing the best search result (as album) for the given query"
  echo "  sp artist <q>                               - Start playing the best search result (as artist) for the given query"
  echo "  sp list <q> ...                             - Start playing the best search result (as playlist) for the given query"
  echo "  sp playlist <q> ...                         - Start playing the best search result (as playlist) for the given query"
  echo "  sp my-list <q> ...                          - Start playing the best search result (as playlist on the user's playlists) for the given query"
  echo "  sp my-playlist <q> ...                      - Start playing the best search result (as playlist on the user's playlists) for the given query"
  echo ""
  echo "  sp play-from <artist name> ...              - Start playing a set of recommendations from the given artists"
  echo ""
  echo "  sp url                                      - Print the HTTP URL for the currently playing track"
  echo "  sp clip                                     - Copy the HTTP URL to the X clipboard"
  echo "  sp http                                     - Open the HTTP URL in a web browser"
  echo ""
  echo "  sp version                                  - Show version information"
  echo "  sp help                                     - Show this information"
  echo ""
  echo "Any other argument will start a search (i.e. 'sp foo' will search for foo)."
}
